package com.epam.rd.autotasks;

import java.util.ArrayList;
import java.util.List;

public class ThreadUnionImplement implements ThreadUnion {
    private static ThreadUnionImplement instance;

    public static ThreadUnionImplement getInstance(String name){
        instance = new ThreadUnionImplement(name);
        return ThreadUnionImplement.instance;
    }
    private String name;
    private int counterAlive = 0;
    private int counter = 0;
    private static boolean shuted = false;
    private boolean finished = false;

    private List<Thread> threads = new ArrayList<>();
    public ThreadUnionImplement(String name){
        this.name = name;
    }


    @Override
    public int totalSize() {
        return counter;
    }

    @Override
    public int activeSize() {
        counterAlive = 0;
        for (Thread th : threads) {
            if (th.isAlive())
                counterAlive++;
        }
        return counterAlive;
    }

    @Override
    public void shutdown() {
        for (Thread th : threads) {
            th.interrupt();
        }
        Thread.currentThread().interrupt();
        shuted = true;
    }

    @Override
    public boolean isShutdown() {
        return shuted;
    }

    @Override
    public void awaitTermination() {
        finished = true;
    }

    @Override
    public boolean isFinished() {
        return finished & shuted;
    }

    @Override
    public List<FinishedThreadResult> results() {
        return null;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Thread th = new Thread(runnable);
        counter++;
        th.setName(name + "-worker-" + (this.totalSize() - 1));
        threads.add(th);
        return th;
    }
    //ss
}
